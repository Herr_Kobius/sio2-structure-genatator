#SiO2_Structure, Atom, Unit_Cell classes of SiO2-Structure-Genatator 
#Copyright (C) 2019 Jacob Lovis Vogler

#The SiO2_Structure_genatator is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

#Version 1.0

class SiO2_Structure():
	def __init__(self, structure_size):
		self.chemical_formula = 'SiO2'
		self.structure_size = structure_size
		self.atom_types = 2
		self.atom_masses = [28.0855, 15.999]
		unit_cell = Unit_Cell()
		self.cell = unit_cell.atoms
		self.atoms = self.place_atoms(structure_size)
        
	def place_atoms(self, structure_size):
		atoms = []
		for x in range(self.structure_size[0]):
			for y in range(self.structure_size[1]):
				for z in range(self.structure_size[2]):
					for atom in self.cell:
						cx = (x*4.978)+atom.position[0]
						cy = (y*4.978)+atom.position[1]
						cz = (z*6.948)+atom.position[2] 
						atoms.append(Atom((cx, cy, cz), atom.type))
		return atoms
			
	@property
	def atom_count(self):
		return len(self.atoms)

	@property
	def bounding_box(self):
		vector_min = (0, 0, 0)
		x, y, z = self.structure_size
		vector_max = (4.978*x, 4.978*y, 6.97285*z)
		return [vector_min, vector_max]

class Atom():
	def __init__(self, position, type):
		self.position = position
		self.type = type

class Unit_Cell():
	def __init__(self):
		self.atoms = [Atom((1.494, 1.494, 0.000), 1), Atom((1.194, 0.514, 1.240), 2),
					Atom((3.484, 3.484, 3.374), 1), Atom((3.784, 4.464, 4.717), 2),
					Atom((0.995, 3.983, 1.737), 1), Atom((1.975, 3.683, 2.977), 2),
					Atom((3.983, 0.995, 5.211), 1), Atom((3.003, 1.295, 6.451), 2),
					Atom((1.295, 3.003, 0.497), 2), Atom((3.683, 1.975, 3.971), 2),
					Atom((0.514, 1.194, 5.708), 2), Atom((4.464, 3.784, 2.234), 2)]
