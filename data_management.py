#data_management module of SiO2-Structure-Genatator 
#Copyright (C) 2019 Jacob Lovis Vogler

#The SiO2_Structure_genatator is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

#Version 1.0

import sys

def create_header(structure):
	lines = []
	empty_line = ''
	chemical_formula = structure.chemical_formula
	x, y, z = structure.structure_size
	atom_count = structure.atom_count
	atom_types = structure.atom_types
	bounding_box = structure.bounding_box

	lines.append('crystalline {} from vmd; x={}, y={}, z={}'.format(chemical_formula, x, y, z))
	lines.append(empty_line)
	lines.append('{}x atoms'.format(atom_count))
	lines.append(empty_line)
	lines.append('{} atom types'.format(atom_types))
	lines.append(empty_line)
	lines.append('{} {} {} {}'.format(bounding_box[0][0], bounding_box[1][0], 'xlo', 'xhi'))
	lines.append('{} {} {} {}'.format(bounding_box[0][1], bounding_box[1][1], 'ylo', 'yhi'))
	lines.append('{} {} {} {}'.format(bounding_box[0][2], bounding_box[1][2], 'zlo', 'zhi'))
	lines.append(empty_line)
	lines.append('Masses')
	lines.append(empty_line)
	lines.append('{}   {}'.format('1', structure.atom_masses[0]))
	lines.append('{}   {}'.format('2', structure.atom_masses[1]))
	lines.append(empty_line)
	lines.append('Atoms')
	lines.append(empty_line)

	return lines

def create_data(structure):
	lines = []
	points = 0
	for atom in structure.atoms:
		x, y, z = atom.position
		lines.append('{} {} {} {} {}'.format(structure.atoms.index(atom)+1, atom.type, x, y, z))
	return lines

def correct_data(structure, hader, data):
	correct = True
	if len(structure.atoms) != structure.structure_size[0]*structure.structure_size[1]*structure.structure_size[2]*12:
		print('Atoms incorrect')
		correct = False
	if len(hader) != 17:
		print('Header incorrect')
		correct = False
	if len(structure.atoms) != len(data):
		print('Data incorrect')
		correct = False
	return correct