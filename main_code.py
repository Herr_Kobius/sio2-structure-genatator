#Main file of SiO2-Structure-Genatator
#Copyright (C) 2019 Jacob Lovis Vogler

#The SiO2_Structure_genatator is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

#Version 1.0

import os
from data_management import create_header, create_data, correct_data
from classes import SiO2_Structure

print('')
print('Dimensions:')
x = input('X: ')
y = input('Y: ')
z = input('Z: ')

structure = SiO2_Structure((int(x), int(y), int(z)))
print('Structure generated(X: {} Y: {} Z: {})'.format(x, y, z))
print('')
header = create_header(structure)
data = create_data(structure)

if correct_data(structure, header, data):
	content = header + data
	correct_path = False
	while not correct_path:
		file_name_input = input('File name: ')
		folde_input = input('Folder name: ')
		print('')
		if file_name_input == '' and folde_input == '':
			file_name = 'structure.txt'
			folder = 'Output'
		try:
			path = os.path.join(folder, file_name)
		except Exception as e:
			print('Path error')
		else:
			correct_path = True
			print('Correct path...')
		
	try:
		with open(path, 'w') as text_file:
			for line in content:
				text_file.write('{}{}'.format(line,'\n'))
	except Exception as e:
		print('Can`t write file')
	else:
		print('File written at {}'.format(path))

else:
	print('File not written')

print('Exiting...')